package fr.umlv.analyser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import fr.umlv.customRTT.CustomRealTimeThread;

/**
 * TODO Refactor
 */
public class RTSJAnalyser {

	private boolean debug = false;

	public static enum PriorityType {
		RM, DM, FP;
	}

	private static int defaultPriority = 0;

	/**
	 * this will contains all the Thread to treat
	 */
	private final List<PriorityRTSJ> processList = new ArrayList<>();
	/**
	 * this is the priority of our process
	 */
	private final PriorityType priority;

	public RTSJAnalyser(PriorityType priority) {
		this.priority = priority;
	}

	/**
	 * this class contains a {@link CustomRealTimeThread} with his priority
	 * 
	 * @author ebabalac
	 * 
	 */
	public class PriorityRTSJ implements Comparable<PriorityRTSJ> {
		int priority;
		CustomRealTimeThread internalRTSJ;

		public PriorityRTSJ(int priority, CustomRealTimeThread internalRTSJ) {

			this.priority = priority;
			this.internalRTSJ = internalRTSJ;
		}

		public CustomRealTimeThread getInternalRTSJ() {
			return internalRTSJ;
		}

		public int getPriority() {
			return priority;
		}

		@Override
		public int compareTo(PriorityRTSJ o) {

			return priority < o.priority ? -1 : priority > o.priority ? 1 : 0;
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof PriorityRTSJ)) {
				return false;
			}
			PriorityRTSJ priorityRTSJ = (PriorityRTSJ) obj;
			return this.getPriority() == priorityRTSJ.getPriority()
					&& this.getInternalRTSJ().equals(
							priorityRTSJ.getInternalRTSJ());
		}
	}

	private double criticalLoad() {

		int n = processList.size();
		return n * (Math.pow(2, 1. / n) - 1);

	}

	/**
	 * charge total consomé
	 * 
	 * @return
	 */
	private double loadSystemConsumes() {
		double u = 0;
		for (PriorityRTSJ priorityRTSJ : processList) {
			CustomRealTimeThread internalRTSJ = priorityRTSJ.getInternalRTSJ();
			u += priorityRTSJ.getInternalRTSJ().getCost() * 1.
					/ priorityRTSJ.getInternalRTSJ().getPeriod();

		}
		return u;
	}

	/**
	 * necessary condition
	 * 
	 * @return
	 */
	private boolean isNecessaryConditionRespect() {
		return loadSystemConsumes() <= 1;
	}

	/**
	 * sufficient condition
	 *
	 * @return
	 */
	private boolean isSufficientConditionRespect() {

		return loadSystemConsumes() <= criticalLoad();
	}

	private boolean withRateMonotonic() {
		/**
		 * si condition necessaire pas verifier le systeme n'est pas faisable
		 */
		if (!isNecessaryConditionRespect()) {
			return false;
		}
		if (checkIfISImplicitTerm()) {
			/**
			 * si condition suffisante verifier pour une tache a echeance
			 * implicite verifier systeme ok
			 */
			if (isSufficientConditionRespect()) {
				return true;
			}

		}
		/**
		 * si cs pas verifier etude de cns
		 */
		return isSufficientAndNecessaryConditionRespectForRMAndDM();

	}

	private boolean checkIfISImplicitTerm() {
		for (PriorityRTSJ priorityRTSJ : processList) {
			if (priorityRTSJ.getInternalRTSJ().getPeriod() != priorityRTSJ
					.getInternalRTSJ().getDeadline()) {

				return false;
			}
		}
		return true;
	}

	private boolean withDeadLineMonotonic() {
		return isNecessaryConditionRespect()
				&& isSufficientAndNecessaryConditionRespectForRMAndDM();
	}

	/**
	 * charge w(t)
	 * 
	 * @param t
	 * @return
	 */
	private int workLoad(CustomRealTimeThread customRealTimeThread, int t,
			int indice) {
		if (indice == 0) {
			indice = 1;
		}
		int w = customRealTimeThread.getCost() * indice;
		// System.out.println(" befor w "+w);
		for (PriorityRTSJ pRTSJ : processList) {
			CustomRealTimeThread customRealTimeThread1 = pRTSJ
					.getInternalRTSJ();
			if (customRealTimeThread1.equals(customRealTimeThread)) {
				break;
			}
			if (debug) {
				System.out.print("w("
						+ t
						+ ") ="
						+ w
						+ "= "
						+ t
						+ " / "
						+ customRealTimeThread1.getPeriod()
						+ " * "
						+ customRealTimeThread1.getCost()
						+ " [t/T] "
						+ getEntiereUpperPart(t
								/ customRealTimeThread1.getPeriod()) + " =");
			}
			w += getEntiereUpperPart(t * 1. / customRealTimeThread1.getPeriod())
					* customRealTimeThread1.getCost();
			if (debug) {
				System.out.println(w);
			}
		}
		return w;
	}

	public int hyperPeriod() {
		int i = 1, result = 0;
		for (PriorityRTSJ priorityRTSJ : processList) {
			result = ppcm(i, priorityRTSJ.getInternalRTSJ().getPeriod());
			i = result;

		}
		return result;
	}

	public int getWorstResponsiveTime(CustomRealTimeThread customRealTimeThread) {
		int oldr, iterationNumber, r, m, index, response = 0;
		if (this.priority == PriorityType.FP) {
			oldr = 0;
			iterationNumber = hyperPeriod();
			r = 1;
			m = response = 0;
			index = 1;

			while (iterationNumber > 0) {

				r = workLoad(customRealTimeThread, r, index);

				// System.out.println(r+"---------- "+customRealTimeThread.getDeadline()+" oldr "+oldr);

				if (r - m >= response) {
					response = r - m;
				}
				// System.out.println("iter "+iterationNumber+ "  m "+m+
				// " index "+index);
				if (r == oldr) {
					// System.out.println("update");
					iterationNumber -= customRealTimeThread.getPeriod();
					m += customRealTimeThread.getPeriod();
					index++;
					r = customRealTimeThread.getPeriod() * (index - 1);

				} else {
					oldr = r;
				}

			}

		} else {

			oldr = 0;
			iterationNumber = 1;
			r = 1;

			while (iterationNumber > 0) {

				r = workLoad(customRealTimeThread, r, 0);

				// System.out.println(r+"---------- "+customRealTimeThread.getDeadline()+" oldr "+oldr);
				if (r >= response) {
					response = r;
				}

				if (r == oldr) {

					iterationNumber--;

				}
				oldr = r;
			}

		}

		return response;
	}

	private boolean isSufficientAndNecessaryConditionRespectForFP() {

		int r = 1, m = 0, index = 1;

		int response = 0;

		int iterationNumber = hyperPeriod();
		for (PriorityRTSJ priorityRTSJ : processList) {

			int oldr = 0;
			iterationNumber = hyperPeriod();
			r = 1;
			m = response = 0;
			index = 1;

			while (iterationNumber > 0) {

				r = workLoad(priorityRTSJ.getInternalRTSJ(), r, index);

				System.out.println(r + "---------- "
						+ priorityRTSJ.getInternalRTSJ().getDeadline()
						+ " oldr " + oldr);

				if (r - m >= response) {
					response = r - m;
				}
				System.out.println("iter " + iterationNumber + "  m " + m
						+ " index " + index);
				if (r == oldr) {
					System.out.println("update");
					iterationNumber -= priorityRTSJ.getInternalRTSJ()
							.getPeriod();
					m += priorityRTSJ.getInternalRTSJ().getPeriod();
					index++;
					r = priorityRTSJ.getInternalRTSJ().getPeriod()
							* (index - 1);

				} else {
					oldr = r;
				}

			}
			response = getWorstResponsiveTime(priorityRTSJ.getInternalRTSJ());
			System.out.println("response = " + response);
			if (response > priorityRTSJ.getInternalRTSJ().getDeadline()) {

				return false;
			}
			System.out.println("\n\n\n");

		}

		return true;
	}

	private boolean isSufficientAndNecessaryConditionRespectForRMAndDM() {
		int r = 1;
		/** set hyperperiode si priorité fp cad arbitraire */
		int iterationNumber = 1;

		for (PriorityRTSJ priorityRTSJ : processList) {
			int oldr = 0;
			iterationNumber = 1;
			r = 1;

			while (iterationNumber > 0) {

				r = workLoad(priorityRTSJ.getInternalRTSJ(), r, 0);
				if (debug) {
					System.out.println(r + "---------- "
							+ priorityRTSJ.getInternalRTSJ().getDeadline()
							+ " oldr " + oldr);
				}
				if (r > priorityRTSJ.getInternalRTSJ().getDeadline())
					return false;

				if (r == oldr) {

					iterationNumber--;

				}
				oldr = r;
			}

		}
		return true;
	}
/**
 * renvoie true si les taches sont faisables en fonction de la priorit� sp�cifi�e
 * @return
 */
	public boolean isFeasible() {
		switch (this.priority) {

		case RM:
			return withRateMonotonic();
		case DM:

			return withDeadLineMonotonic();
		case FP:
			return withFixPrioriry();

		}

		return false;
	}

	private boolean withFixPrioriry() {
		return isNecessaryConditionRespect()
				&& isSufficientAndNecessaryConditionRespectForFP();
	}

	private int ppcm(int Nb1, int Nb2) {
		int Produit, Reste, result;

		Produit = Nb1 * Nb2;
		Reste = Nb1 % Nb2;
		while (Reste != 0) {
			Nb1 = Nb2;
			Nb2 = Reste;
			Reste = Nb1 % Nb2;
		}
		result = Produit / Nb2;

		return result;
	}

	private static int getEntiereUpperPart(double b) {
		double r = b - (int) b;
		return r == 0 ? (int) b : (int) b + 1;

	}

	/**
	 * add a task : in rate monotonic or deadline monotonic no priority is
	 * needed but if its fixed priority it added a random priority to use a
	 * controlled fixed priority use to add task the other function add task
	 * 
	 * @param customRealTimeThread1
	 */
	public void addTask(CustomRealTimeThread customRealTimeThread1) {

		switch (this.priority) {

		case RM:
			this.processList.add(new PriorityRTSJ(customRealTimeThread1
					.getPeriod(), customRealTimeThread1));
			break;
		case DM:
			this.processList.add(new PriorityRTSJ(customRealTimeThread1
					.getDeadline(), customRealTimeThread1));
			break;
		case FP:
			this.processList.add(new PriorityRTSJ(defaultPriority++,
					customRealTimeThread1));
			break;
		}

		Collections.sort(processList);
	}

	/**
	 * add a task in fixed priority but the analyzer is set for deadline
	 * monotonci rule or fixed priority rule the given priority will be ignore
	 * and auto calculate by the analyzer
	 * 
	 * @param customRealTimeThread1
	 * @param priority
	 */
	public void addTask(CustomRealTimeThread customRealTimeThread1, int priority) {
		switch (this.priority) {

		case RM:
			this.processList.add(new PriorityRTSJ(customRealTimeThread1
					.getPeriod(), customRealTimeThread1));
			break;
		case DM:
			this.processList.add(new PriorityRTSJ(customRealTimeThread1
					.getDeadline(), customRealTimeThread1));
			break;
		case FP:
			this.processList.add(new PriorityRTSJ(priority,
					customRealTimeThread1));
			break;
		}

		Collections.sort(processList, new Comparator<PriorityRTSJ>() {
			@Override
			public int compare(PriorityRTSJ o1, PriorityRTSJ o2) {
				return o1.getPriority() > o2.getPriority() ? -1 : o1
						.getPriority() == o2.getPriority() ? 0 : 1;
			}
		});
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		int i = 1;

		int priority2;
		for (PriorityRTSJ rtsj : processList) {
			CustomRealTimeThread internalRTSJ = rtsj.internalRTSJ;
			if (this.priority == PriorityType.FP) {
				priority2 = rtsj.getPriority();
				builder.append("index = " + i + " cost= "
						+ internalRTSJ.getCost() + " period "
						+ internalRTSJ.getPeriod() + " deadline "
						+ internalRTSJ.getDeadline() + " priorite = "
						+ priority2 + "\n");
			} else {
				builder.append("index = " + i + " cost= "
						+ internalRTSJ.getCost() + " period "
						+ internalRTSJ.getPeriod() + " deadline "
						+ internalRTSJ.getDeadline() + "\n");
			}
			i++;
		}
		return builder.toString();
	}

	/**
	 * Analyse de sensibilit� effectu�e en ajout de temps egalitaire
	 * 
	 * @return
	 */
	public int getSystemSensibility() {
		int addedCost = 0;
		while (isFeasible()) {
			for (PriorityRTSJ priorityRTSJ : this.processList) {
				CustomRealTimeThread internalRTSJ = priorityRTSJ.internalRTSJ;
				internalRTSJ.setCost(internalRTSJ.getCost() + 1);

			}
			addedCost += 1;
		}
		return addedCost - 1;
	}

	public PriorityType getPriority() {
		return priority;
	}

	public CustomRealTimeThread getCustomRealTimeThread(int i) {

		return this.processList.get(i).internalRTSJ;
	}

	// public static void main(String[] args) {
	// List<PriorityRTSJ> processList = new ArrayList<>();
	// RTSJAnalyser rtsjAnalyser = new RTSJAnalyser(PriorityType.DM);
	// CustomRealTimeThread customRealTimeThread1 = new CustomRealTimeThread(
	// 3, 7, 7);
	// CustomRealTimeThread customRealTimeThread2 = new CustomRealTimeThread(
	// 3, 11, 11);
	// CustomRealTimeThread customRealTimeThread3 = new CustomRealTimeThread(
	// 3, 20, 20);
	// // CustomRealTimeThread customRealTimeThread3 = new
	// // CustomRealTimeThread(5,17,17);
	// rtsjAnalyser.addTask(customRealTimeThread1);
	// rtsjAnalyser.addTask(customRealTimeThread2);
	// rtsjAnalyser.addTask(customRealTimeThread3);
	// // System.out.println(rtsjAnalyser.ppcm(12,8));
	//
	// // for (PriorityRTSJ i : rtsjAnalyser.processList) {
	// // System.out.println("--> " + i.getPriority() + "   "
	// // + rtsjAnalyser.getWorstResponsiveTime(i.getInternalRTSJ()));
	// // }
	//
	// System.out.println(rtsjAnalyser);
	// CustomRealTimeThread customRealTimeThread = rtsjAnalyser
	// .getCustomRealTimeThread(2);
	// System.out.println(customRealTimeThread.getPeriod());
	//
	// //
	// //
	// // System.out.println(rtsjAnalyser.isFeasible());
	// // System.out.println("===> "+rtsjAnalyser.hyperPeriod());
	// /*
	// * int a = 15; double b = 15.25;
	// * System.out.println(getEntiereUpperPart(15.1));
	// */
	// }

}
