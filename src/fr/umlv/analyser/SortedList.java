package fr.umlv.analyser;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
/**
 * liste triable
 * @author Emmanuel
 *
 */
public class SortedList extends AbstractList<Integer> {

	private final List<Integer> internalList = new ArrayList<>();
	
	private class PriorityRTSJ{
		int priority;
		RTSJAnalyser internalRTSJ;
		public PriorityRTSJ(int priority, RTSJAnalyser internalRTSJ) {
		
			this.priority = priority;
			this.internalRTSJ = internalRTSJ;
		}
		
		
		public RTSJAnalyser getInternalRTSJ() {
			return internalRTSJ;
		}
		public int getPriority() {
			return priority;
		}
		
		
		
	}
	@Override
	public Integer get(int index) {

		return internalList.get(index);
	}

	@Override
	public int size() {

		return internalList.size();
	}
	
	@Override
	public boolean add(Integer e) {
		internalList.add(e);
		 Collections.sort(internalList, new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				
				return o1<o2 ? 1 : o1 > o2 ? -1 :0;
			}
		});
		 return true;
	}

}
