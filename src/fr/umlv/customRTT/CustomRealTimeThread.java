package fr.umlv.customRTT;

import java.util.UUID;

/**
 * Represente une tache temps reel avec son cout preiode et echeance chaque
 * tache est identifiée pr un ID unique
 * 
 * @author Emmanuel
 *
 */
public class CustomRealTimeThread {

	private int cost;
	private int period;
	private int deadline;
	private final UUID ID = UUID.randomUUID();

	public CustomRealTimeThread(int cost, int period, int deadline) {
		this.cost = cost;
		this.period = period;
		this.deadline = deadline;
	}

	public int getCost() {
		return cost;
	}

	public int getPeriod() {
		return period;
	}

	public int getDeadline() {
		return deadline;
	}

	public UUID getID() {
		return ID;
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof CustomRealTimeThread)) {
			return false;
		}
		CustomRealTimeThread o = (CustomRealTimeThread) obj;
		return this.ID.equals(o.getID());
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
}
