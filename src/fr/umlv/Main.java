package fr.umlv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import fr.umlv.analyser.RTSJAnalyser;
import fr.umlv.analyser.RTSJAnalyser.PriorityType;
import fr.umlv.customRTT.CustomRealTimeThread;

public class Main {
	public static void main(String[] args) throws IOException {
		System.out.println("Bienvenue dans l'analyseur de tache temps reel");

		RTSJAnalyser rtsjAnalyser = createAnalyser();
		addThreadToAnalyze(rtsjAnalyser);
		System.out.println(rtsjAnalyser);
		chooseOption(rtsjAnalyser);

	}

	private static void chooseOption(RTSJAnalyser rtsjAnalyser)
			throws IOException {
		System.out.println("Entrer \n"
				+ "1 Pour lancer un teste de faisabilit�\n"
				+ "2 Pour recuperer le pire temps de reponse\n"
				+ "3 Pour une analyse de sensibilit�");
		int choix = 0;

		BufferedReader bufferReader = new BufferedReader(new InputStreamReader(
				System.in));
		boolean choice = true;

		choix = Integer.parseInt(bufferReader.readLine());

		switch (choix) {
		case 1:
			boolean result = rtsjAnalyser.isFeasible();
			if (result) {
				System.out.println("Le systeme est faisable");
			} else {
				System.out.println("le syst�me n'est pas faisable");
			}
			break;
		case 2:
			System.out
					.println("choissisez l'index de la tache dont vous voulez obtenir une analyse ATR");
			System.out.println("Liste des taches ");
			System.out.println(rtsjAnalyser);
			int index = Integer.parseInt(bufferReader.readLine());
			CustomRealTimeThread customRealTimeThread = rtsjAnalyser
					.getCustomRealTimeThread(index - 1);
			System.out
					.println("Pire temps reponse = "
							+ rtsjAnalyser
									.getWorstResponsiveTime(customRealTimeThread));
			break;
		case 3:
			System.out.println("Sensibilite = "
					+ rtsjAnalyser.getSystemSensibility());
		default:
			break;
		}

	}

	/**
	 * ajoute des taches a l' analyseur
	 * 
	 * @param rtsjAnalyser
	 * @throws IOException
	 */
	private static void addThreadToAnalyze(RTSJAnalyser rtsjAnalyser)
			throws IOException {
		int cost, period, deadline, priority;
		System.out
				.println("veuiller entrer vos taches\n"
						+ "a la fin de chaque tache, il vous sera demander si vous voulez arreter la creation ou en ajouter un autre");
		boolean continu = true;
		BufferedReader bufferReader = new BufferedReader(new InputStreamReader(
				System.in));
		while (continu) {
			System.out.println("ENTRER LE COUT  :");
			cost = Integer.parseInt(bufferReader.readLine());
			System.out.println("ENTRER LA PERIOD  :");
			period = Integer.parseInt(bufferReader.readLine());
			System.out.println("ENTRER LA DEADLINE :");
			deadline = Integer.parseInt(bufferReader.readLine());
			if (rtsjAnalyser.getPriority() == PriorityType.FP) {
				System.out.println("ENTRER La Priorit�  :");
				priority = Integer.parseInt(bufferReader.readLine());
				rtsjAnalyser.addTask(new CustomRealTimeThread(cost, period,
						deadline), priority);
			} else {
				rtsjAnalyser.addTask(new CustomRealTimeThread(cost, period,
						deadline));

			}

			System.out.println("\n voulez-vous entrer une autre tache \n"
					+ "entrer [o,y,  oui,yes] pour oui ou \n"
					+ " n pour quitter");
			String readLine = bufferReader.readLine();
			if (!(readLine.equalsIgnoreCase("o")
					|| readLine.equalsIgnoreCase("y")
					|| readLine.equalsIgnoreCase("oui") || readLine
						.equalsIgnoreCase("yes"))) {
				continu = false;
			}

		}

	}

	/**
	 * cree l'analyseur en choisissant le type d'ordonnancement
	 * 
	 * @return
	 * @throws IOException
	 */
	private static RTSJAnalyser createAnalyser() throws IOException {

		System.out.println(" veuillez entrer \n"
				+ "1 : Pour Analyseur de tache RateMonotonic\n"
				+ "2 : Pour Analyseur de tache DeadLineMonotonic\n"
				+ "3 : Pour Analyseur de tache � priorit� fixe\n");
		int choix = 0;

		boolean choice = true;
		BufferedReader bufferReader = new BufferedReader(new InputStreamReader(
				System.in));

		while (choice) {
			choix = Integer.parseInt(bufferReader.readLine());

			switch (choix) {
			case 1:
				return new RTSJAnalyser(PriorityType.RM);

			case 2:
				return new RTSJAnalyser(PriorityType.DM);

			case 3:
				return new RTSJAnalyser(PriorityType.FP);

			default:
				System.err.println("L'option entrer n'est pas reconnu \n"
						+ " veuillez entrer \n"
						+ "1 : Pour Analyseur de tache RateMonotonic\n"
						+ "2 : Pour Analyseur de tache DeadLineMonotonic\n"
						+ "3 : Pour Analyseur de tache � priorit� fixe\n");

			}
		}
		throw new IllegalStateException();
	}
}
